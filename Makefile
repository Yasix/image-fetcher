php:
	docker compose exec --user=www-data php-nodebug sh

php_xd:
	docker compose exec --user=www-data php sh

build:
	docker compose build

up:
	docker compose up -d

down:
	docker compose down

worker:
	docker compose exec --user=www-data php-nodebug bin/console mes:cons async -vv --failure-limit=10

setup:
	docker compose build
	docker compose up -d
	docker compose exec --user=www-data php-nodebug composer install
	docker compose exec --user=www-data php-nodebug bin/console do:mi:mi

