<?php

namespace App\Controller;

use App\Entity\ImageFetchTask;
use App\Form\ImageFetchTaskType;
use App\Message\ImageFetchTaskCreatedMessage;
use App\Repository\ImageFetchTaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/image/fetch/task')]
class ImageFetchTaskController extends AbstractController
{
    #[Route('/', name: 'app_image_fetch_task_index', methods: ['GET'])]
    public function index(ImageFetchTaskRepository $imageFetchTaskRepository): Response
    {
        return $this->render('image_fetch_task/index.html.twig', [
            'image_fetch_tasks' => $imageFetchTaskRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_image_fetch_task_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $entityManager,
        MessageBusInterface $bus,
    ): Response
    {
        $imageFetchTask = new ImageFetchTask();
        $form = $this->createForm(ImageFetchTaskType::class, $imageFetchTask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFetchTask->setStatus(ImageFetchTask::STATUS_NEW);
            $entityManager->persist($imageFetchTask);
            $entityManager->flush();

            $bus->dispatch(new ImageFetchTaskCreatedMessage($imageFetchTask->getId()));

            $this->addFlash('info', 'Task added');

            return $this->redirectToRoute('app_image_fetch_task_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('image_fetch_task/new.html.twig', [
            'image_fetch_task' => $imageFetchTask,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_image_fetch_task_show', methods: ['GET'])]
    public function show(ImageFetchTask $imageFetchTask): Response
    {
        return $this->render('image_fetch_task/show.html.twig', [
            'image_fetch_task' => $imageFetchTask,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_image_fetch_task_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ImageFetchTask $imageFetchTask, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ImageFetchTaskType::class, $imageFetchTask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_image_fetch_task_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('image_fetch_task/edit.html.twig', [
            'image_fetch_task' => $imageFetchTask,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_image_fetch_task_delete', methods: ['POST'])]
    public function delete(Request $request, ImageFetchTask $imageFetchTask, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$imageFetchTask->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($imageFetchTask);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_image_fetch_task_index', [], Response::HTTP_SEE_OTHER);
    }
}
