<?php

namespace App\Message;

final class ImageFetchTaskCreatedMessage
{
    public function __construct(
        public string $taskId
    )
    {
    }
}
