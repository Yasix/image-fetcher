<?php

namespace App\MessageHandler;

use App\Entity\ImageFetchTask;
use App\Message\ImageFetchTaskCreatedMessage;
use App\Repository\ImageFetchTaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class ImageFetchTaskCreatedMessageHandler
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ImageFetchTaskRepository $taskRepository,
        private readonly ClientInterface $http,
        private readonly LoggerInterface $logger,
    )
    {
    }

    public function __invoke(ImageFetchTaskCreatedMessage $message): void
    {
        $task = $this->taskRepository->find($message->taskId);
        $task->setStatus(ImageFetchTask::STATUS_PROGRESS);
        $this->entityManager->flush();
        $response = $this->http->request(Request::METHOD_GET, $task->getUrl());
        $page = $response->getBody()->getContents();

        preg_match_all('#"([^\"]+\.(?:jpg|jpeg|png|svg|gif))"#', $page, $m);

        $base = $task->getUrl();
        $errors = 0;
        $totalSize = 0;
        $data = [];
        foreach ($m[1] as $path) {
            $path = $this->convertUnicodeEntities($path);
            try {
                $size = intval($this->http->request(Request::METHOD_HEAD, $base . $path)->getHeader('Content-Length')[0] ?? 0);
                if (!$size) {
                    $this->logger->warning('Zero size image', ['base' => $base, 'path' => $path]);
                }
                $totalSize += $size;
                $data['images'][] = ['path' => $path, 'size' => $size];
            }
            catch (GuzzleException $e) {
                $this->logger->error($e->getMessage(), ['base' => $base, 'path' => $path]);
                $errors++;
            }
        }
        $data['total_size'] = $totalSize;
        $data['error_count'] = $errors;
        $task->setData($data);
        $task->setStatus(ImageFetchTask::STATUS_READY);
        $this->entityManager->flush();
    }

    private function convertUnicodeEntities($str): string
    {
        return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $str);
    }
}
